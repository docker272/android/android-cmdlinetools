FROM openjdk:17-jdk-slim

ARG OPT_DIR=
ARG ANDROID_SDK_ROOT=

# versions
ARG VERSION=
ARG ANDROID_COMMANDLINE_TOOLS=
ARG GRADLE_VERSION=

# labels
LABEL version = "${VERSION}"
LABEL description = "android-sdk@${ANDROID_COMMANDLINE_TOOLS} + gradle@${GRADLE_VERSION}"

# env
ENV GRADLE_HOME "${OPT_DIR}/gradle-${GRADLE_VERSION}"
ENV ANDROID_SDK_ROOT "${ANDROID_SDK_ROOT}"
ENV COMMANDLINETOOLS_LATEST_DIR "${ANDROID_SDK_ROOT}/cmdline-tools/latest"
ENV PATH "$PATH:${COMMANDLINETOOLS_LATEST_DIR}/bin:${GRADLE_HOME}/bin"

# files
ARG DOWNLOAD_FILE_ANDROID_COMMANDLINE_TOOLS="commandlinetools-linux-${ANDROID_COMMANDLINE_TOOLS}_latest.zip"
ARG DOWNLOAD_FILE_GRADLE="gradle-${GRADLE_VERSION}-bin.zip"

# sha256
ARG ANDROID_DOWNLOAD_SHA256=
ARG GRADLE_DOWNLOAD_SHA256=

# download urls
ARG DOWNLOAD_URL_ANDROID_COMMANDLINE_TOOLS="https://dl.google.com/android/repository/${DOWNLOAD_FILE_ANDROID_COMMANDLINE_TOOLS}"
ARG DOWNLOAD_URL_GRADLE="https://services.gradle.org/distributions/${DOWNLOAD_FILE_GRADLE}"

# apt
RUN apt-get --quiet update --yes \
  && apt-get upgrade --yes \
  && apt-get --quiet install --yes --no-install-recommends lib32stdc++6 lib32z1 curl unzip

# check and extract
RUN curl -L ${DOWNLOAD_URL_ANDROID_COMMANDLINE_TOOLS} --output ${OPT_DIR}/${DOWNLOAD_FILE_ANDROID_COMMANDLINE_TOOLS} \
  && echo "${ANDROID_DOWNLOAD_SHA256} ${OPT_DIR}/${DOWNLOAD_FILE_ANDROID_COMMANDLINE_TOOLS}" | sha256sum --check --status \
  && unzip -q ${OPT_DIR}/${DOWNLOAD_FILE_ANDROID_COMMANDLINE_TOOLS} -d ${ANDROID_SDK_ROOT} \
  && rm ${OPT_DIR}/${DOWNLOAD_FILE_ANDROID_COMMANDLINE_TOOLS} \
  && mkdir ${COMMANDLINETOOLS_LATEST_DIR} \
  && mv ${ANDROID_SDK_ROOT}/cmdline-tools/bin ${COMMANDLINETOOLS_LATEST_DIR} \
  && mv ${ANDROID_SDK_ROOT}/cmdline-tools/lib ${COMMANDLINETOOLS_LATEST_DIR} \
  && mv ${ANDROID_SDK_ROOT}/cmdline-tools/NOTICE.txt ${COMMANDLINETOOLS_LATEST_DIR} \
  && mv ${ANDROID_SDK_ROOT}/cmdline-tools/source.properties ${COMMANDLINETOOLS_LATEST_DIR}

RUN curl -L ${DOWNLOAD_URL_GRADLE} --output ${OPT_DIR}/${DOWNLOAD_FILE_GRADLE} \
  && echo "${GRADLE_DOWNLOAD_SHA256} ${OPT_DIR}/${DOWNLOAD_FILE_GRADLE}" | sha256sum --check --status \
  && unzip -q ${OPT_DIR}/${DOWNLOAD_FILE_GRADLE} -d ${OPT_DIR} \
  && rm ${OPT_DIR}/${DOWNLOAD_FILE_GRADLE}

RUN yes | sdkmanager --licenses > /dev/null

# apt clean
RUN rm -rf /var/lib/apt/lists/*
RUN apt-get clean